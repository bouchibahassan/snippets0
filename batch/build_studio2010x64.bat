set BUILD_DIR=.build_vstudio2010

IF EXIST %BUILD_DIR% (rd /S /Q %BUILD_DIR%)
mkdir %BUILD_DIR%
cd %BUILD_DIR%

cmake .. -G "Visual Studio 10 2010 Win64"

cd ..
PAUSE