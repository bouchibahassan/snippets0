:: Batch script file to copy recursively all XYZ files in Semis_points folders 
:: in a destination folders
:: Hassan Bouchiba
:: 21/10/20115

SET destination_folder=semis_points

MD %destination_folder%
FOR /R %%G IN (Semis_points\*.xyz) DO COPY /B %%G %destination_folder%

PAUSE